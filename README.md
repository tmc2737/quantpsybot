![](https://bytebucket.org/tmc2737/quantpsybot/raw/fa1bf24dd53b15e4e6f2c9c0246a36e8938fa3e1/quantpsy.png?token=955efd094a9e22ebe5e60595b424e2de7b01dbf7)

# @quantpsybot (Twitter)

This bot scrapes different websites for new releases in journal articles related to Quantitative Psychology. 
Currently, we are scraping from:
- Quantitative Psychology and Measurement
- Applied Psychological Measurement
- Psychological Methods
- Psychological Assessment
- British Journal of Mathematical and Statistical Psychology
- Journal of Mathematical Psychology


Sadly, there are very few Quantitative Psychology journals out there, but we will be deploying code that searches for relevant articles in other journals in the near future.

The quantpsybot is the first iteration of a series of Twitterbots dedicated to mining journal websites and is directly related to @cogpsybot and @cogagingbot.

Follow us on Twitter ([@quantpsybot](http://www.twitter.com/quantpsybot))!

***

###### UPDATE (08/15/16):
- We will be updating the code to delay Tweets. Many publishing companies publish their newest articles in bulk, which causes the bots to post at once, potentially violating Twitter's API guidelines. It's also annoying to see 10 tweets published at once.
